<?php

// Contrauctor dan Destructor
echo "<h1>Tugas pendalaman OOP PHP</h1>";


// Parent class
abstract class Hewan {
    public $name;
    public $darah = 50;
    public $jumlahkaki;
    public $keahlian;

    function atraksi(){
        return "Atraksi = " . $this->name ." sedang " .$this->keahlian;
       }

    public function __construct($name) {
      $this->name = $name;
    }
    abstract public function intro();
}    
abstract class Fight extends Hewan
{
    public $attackPower;
    public $defencekPower;

}
  
  // Child classes
  class Harimau extends Hewan {
    public function intro() : string {
      return "Nama Hewan = $this->name!";
    }
    public $jumlahkaki = 4;
    public $keahlian = "Lari Cepat";
    public $attackPower = 7;
    public $defencekPower = 8;
  }

  class Elang extends Hewan {
    public function intro() : string {
      return "Nama Hewan = $this->name";
    }
    public $jumlahkaki = 2;
    public $keahlian = "Terbang Tinggi";
    public $attackPower = 10;
    public $defencekPower = 5;
  }
  
  
  // Create objects from the child classes
  $harimau = new harimau("Harimau");
  echo $harimau->intro();
  echo "<br>";
  echo "Darah =" .$harimau->darah ."<br>";
  echo "Jumlah kaki =" .$harimau->jumlahkaki ."<br>";
  echo "Keahlian =$harimau->keahlian";
  echo "<br>";
  echo "Attak Power =" .$harimau->attackPower ."<br>";
  echo "<br>" ."<br>";
  echo $harimau->atraksi();
  echo "<br>" ."<br>";

  $elang = new elang("Elang");
  echo $elang->intro();
  echo "<br>";
  echo "Darah = " .$elang->darah ."<br>";
  echo "Jumlah kaki = " .$elang->jumlahkaki ."<br>";
  echo "Keahlian = $elang->keahlian";
  echo "<br>";
  echo "Attak Power = " .$elang->attackPower ."<br>";
  echo "<br>" ."<br>";


  
  ?>